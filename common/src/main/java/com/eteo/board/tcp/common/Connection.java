package com.eteo.board.tcp.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

public class Connection implements Runnable {
	ConnectionListener connectionListener;

	InputStream ois;
	OutputStream oos;

	boolean runnable = true;

	boolean active = true;

	public Connection(Socket socket, ConnectionListener connectionListener) {
		this.connectionListener = connectionListener;
		try {
			oos = socket.getOutputStream();
			ois = socket.getInputStream();
			new Thread(this).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Object byteArrayToObject(byte[] data) {
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(bis);
			return in.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public static byte[] objectToByteArray(Object object) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(object);
			out.flush();
			return bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public void send(byte[] data) {
		try {
			oos.write(data);
		} catch (SocketException e) {
			connectionListener.disposeConnection(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			byte[] buffer = new byte[1024];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while (runnable) {
				baos.reset();
				int i = ois.read(buffer);
				baos.write(buffer, 0, i);
				if (active) {
					connectionListener.processMessage(this, baos.toByteArray());
				} else {
					synchronized (this) {
						wait();
					}
				}
			}
			oos.close();
			ois.close();
		} catch (SocketException | EOFException e) {
			connectionListener.disposeConnection(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setRunnable(boolean runnable) {
		this.runnable = runnable;
	}

	public synchronized void dispose() {
		runnable = false;
		send(null);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
		if (active) {
			synchronized (this) {
				notify();
			}
		}
	}
}