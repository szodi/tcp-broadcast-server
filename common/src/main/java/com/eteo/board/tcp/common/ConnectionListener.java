package com.eteo.board.tcp.common;

public interface ConnectionListener {
	void processMessage(Connection connection, byte[] data);

	void disposeConnection(Connection connection);
}