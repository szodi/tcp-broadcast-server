package com.eteo.board.tcp.entity;

import java.io.Serializable;

public class SerializablePoint implements Serializable {

	private static final long serialVersionUID = 3490097124123937079L;
	
	double x;
	double y;

	public SerializablePoint(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "SerializablePoint [x=" + x + ", y=" + y + "]";
	}
}
