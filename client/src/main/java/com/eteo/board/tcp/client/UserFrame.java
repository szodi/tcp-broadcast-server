package com.eteo.board.tcp.client;

import java.net.SocketException;

import com.eteo.board.tcp.common.Connection;
import com.eteo.board.tcp.common.ConnectionFactory;
import com.eteo.board.tcp.common.ConnectionListener;
import com.eteo.board.tcp.entity.SerializablePoint;

import javafx.application.Platform;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class UserFrame extends AnchorPane implements ConnectionListener {

	SerializablePoint pressed;
	Connection connection;

	public UserFrame() {
		try {
			connection = ConnectionFactory.getConnection("localhost", 6789, this);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		setMinSize(100, 100);
		setMaxSize(300, 300);

		addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
			pressed = new SerializablePoint(event.getX(), event.getY());
		});

		addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
			SerializablePoint point = new SerializablePoint(getTranslateX() + event.getX() - pressed.getX(),
					getTranslateY() + event.getY() - pressed.getY());
			setTranslateX(point.getX());
			setTranslateY(point.getY());
			connection.send(Connection.objectToByteArray(point));
		});
	}

	public Connection getConnection() {
		return connection;
	}

	@Override
	public void processMessage(Connection connection, byte[] data) {
		if (data != null) {
			Platform.runLater(() -> {
				SerializablePoint point = (SerializablePoint) Connection.byteArrayToObject(data);
				setTranslateX(point.getX());
				setTranslateY(point.getY());
			});
		}
	}

	@Override
	public void disposeConnection(Connection connection) {
	}
}
