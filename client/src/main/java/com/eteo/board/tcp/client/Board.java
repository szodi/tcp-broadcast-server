package com.eteo.board.tcp.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Board extends Application {

	private UserFrame userFrame;

	@Override
	public void start(Stage primaryStage) throws Exception {
		Group group = new Group();

		userFrame = new UserFrame();
		group.getChildren().add(userFrame);
		primaryStage.setScene(new Scene(group, 800, 800));
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });		
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
